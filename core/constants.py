import os
from pathlib import Path
from whoosh.classify import *
from whoosh.qparser import AndGroup

# Get relative path to the current module
MOD_PATH = Path(__file__).parent

DEBUG = True
MODEL_VERSION = 2  # 1 - InferSent1 + GloVe, #2 - InferSent2 + FastText
MODEL_PARAMS = {'bsize': 64, 'word_emb_dim': 300, 'enc_lstm_dim': 2048,
                'pool_type': 'max', 'dpout_model': 0.0, 'version': MODEL_VERSION}

FILTER_BY_KEYWORDS = True
USE_WHOOSH = True
MODEL = Bo1Model
EXPANSION_MODEL = Bo1Model
GROUP = AndGroup
KEYWORD_THRESHOLD = 13
EXPAND_KEYWORDS = False
KEYWORD_EXPANDER_N = 2
COMPUTE_SIMILARITY = False
# Similarity calculation method, if not BERT, defaults to InferSent. If using BERT, must run the bert-as-a-service server
USE_BERT_FOR_SIMILARITY = False

# Dependency paths
DATASET_PATH = os.getenv('DATASET_PATH')

if DATASET_PATH is None:
    raise ValueError("DATASET_PATH environment variable not set!")

SENTENCE_HANDLER_WV_PATH = '{}/GloVe/glove.840B.300d.txt'.format(
    DATASET_PATH) if MODEL_VERSION == 1 else '{}/fastText/crawl-300d-2M.vec'.format(DATASET_PATH)

KEYWORD_EXPANDER_WV_PATH = '{}/GloVe/glove.840B.300d.w2v.txt'.format(DATASET_PATH) \
    if EXPAND_KEYWORDS else None

RESULTS_FILEPATH = (MOD_PATH / 'results.csv').resolve()

# Dataset paths
QUESTIONS_FILEPATH = (MOD_PATH / "../datasets/questions.csv").resolve()
ANSWERS_FILEPATH = (MOD_PATH / "../datasets/answers.csv").resolve()
RELEVANCY_SCORES_FILEPATH = (MOD_PATH / "../datasets/relevances.csv").resolve()
