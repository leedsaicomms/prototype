from datetime import datetime
from string import Template
from whoosh.classify import *
from whoosh.qparser import OrGroup, AndGroup

import pandas as pd
import numpy as np

# Constants
from constants import *

# Model settings
from keyword_expansion import KeywordExpander, DummyKeywordExpander
from response_search import ResponseSearcher
from sentence_handling import SentenceHandler, DummySentenceHandler
from whoosh_search_engine import WhooshSearchEngine

# A all_responses of responses, which is a CSV file in this prototype
all_responses = pd.read_csv(ANSWERS_FILEPATH, encoding="utf-8").text

# Initialize necessary components
sentence_handler = SentenceHandler(SENTENCE_HANDLER_WV_PATH, MODEL_PARAMS) if not USE_WHOOSH else None
keyword_expander = KeywordExpander(KEYWORD_EXPANDER_WV_PATH, KEYWORD_EXPANDER_N) if not USE_WHOOSH else None
response_searcher = ResponseSearcher(all_responses)
search_engine = WhooshSearchEngine()

csv_template = Template("${whoosh_extraction_model},${whoosh_expansion_model},${whoosh_query_grouping},"
                        "${keyword_threshold},${keyword_expander_n},${total_precision},${total_recall},${total_f1},"
                        "${mean_average_precision},${mean_reciprocal_rank},${normalised_discounted_cumulative_gain},"
                        "${mean_absolute_error}\n")


def compare_from_files(questions_filepath, answers_filepath, relevancy_scores_filepath, filter_n, expander_n, whoosh_extraction_model, whoosh_expansion_model, whoosh_query_grouping):
    """
    Compares similarities of all questions together with all of their answers.

    :param questions_filepath: file path of questions dataset
    :param answers_filepath: file path of answers dataset
    :param relevancy_scores_filepath: file path of relevancy scores dataset
    :param filter_n: number of keywords to extract from questions (used for writing results CSV file)
    :param expander_n: number of keywords generated for each extracted keyword (used for writing results CSV file)
    """
    questions_df = pd.read_csv(questions_filepath, index_col='id')
    answers_df = pd.read_csv(answers_filepath, header=0,
                             index_col='id')
    relevancy_scores_df = pd.read_csv(relevancy_scores_filepath, header=0,
                                      dtype={'question_id': 'category', 'answer_id': 'category', 'score': 'float'})

    target_qid2a = {question_tp.Index: [] for question_tp in questions_df.itertuples()}

    total_tp, total_tn, total_fp, total_fn = 0, 0, 0, 0

    relevancy_scores_with_answer_text = []
    mean_absolute_error_sums, mean_average_precision_sums, mean_reciprocal_rank_sums, \
        normalised_discounted_cumulative_gain_sums = 0, 0, 0, 0

    for relevancy_score_tp in relevancy_scores_df.itertuples():

        # Obtain Question ID
        question_id = int(relevancy_score_tp.question_id)

        # Obtain response ID and text
        answer_id = int(relevancy_score_tp.answer_id)
        answer = answers_df.loc[answer_id, 'text']

        # Obtain relevancy score
        relevancy_score = float(relevancy_score_tp.score)

        # Relevancy scores range from 0.0 to 1.0 (with possible values being 0.0, 0.5 and 1.0)
        # If the relevancy score for the given question-answer pair is 1.0 (the highest possible relevancy score,
        # we add it to the list of possible target responses for the given question.
        if relevancy_score == 1.0:
            target_qid2a[question_id].append(answer)

        # Mean absolute error - Create a list of of records with Q ID, answer (text) and (actual relevancy) score
        relevancy_scores_with_answer_text.append({'question_id': question_id, 'response': answer, 'score': relevancy_score})

    for question_tp in questions_df.itertuples():
        question = question_tp.text
        question_id = question_tp.Index

        # Default if keyword extraction/expansion are disabled.
        filtered_responses = list(all_responses)

        if FILTER_BY_KEYWORDS and filter_n > 0:
            # Extract keywords
            if USE_WHOOSH:
                keywords = search_engine.extract_keywords(question, numterms=numterms_initial_query, model=model)
            else:
                keywords = sentence_handler.extract_keywords(question)

            if EXPAND_KEYWORDS and expander_n > 0:
                # Expand keywords
                if USE_WHOOSH:
                    keywords = search_engine.expand_keywords(keywords, numterms=numterms_query_expansion, group=group, model=model)
                else:
                    keywords = keyword_expander.expand(keywords)

            # Use keyword search to filter out a subset of responses
            filtered_responses = response_searcher.search(keywords)

        # Default if similarity computation is disabled
        top_5_responses = filtered_responses[:5]
        top_5_responses_with_score = []
        for response in top_5_responses:
            top_5_responses_with_score.append({"response": response, "score": 1})

        if COMPUTE_SIMILARITY and filtered_responses:
            # Calculate similarities
            if USE_BERT_FOR_SIMILARITY:
                responses = get_top_5_responses_bert(filtered_responses, question)
            else:
                responses = get_top_5_responses_infersent(filtered_responses, question)
            top_5_responses = responses[0]
            top_5_responses_with_score = responses[1]

        target_answers = set(target_qid2a[question_id])

        # Calculate metrics for question, update running sums (used to calculate overall metric average for system)
        query_average_precision, query_reciprocal_rank, normalised_discounted_cumulative_gain, mean_absolute_error = \
            calculate_metrics(top_5_responses_with_score, relevancy_scores_with_answer_text, question_id)

        mean_average_precision_sums += query_average_precision
        mean_reciprocal_rank_sums += query_reciprocal_rank
        normalised_discounted_cumulative_gain_sums += normalised_discounted_cumulative_gain
        mean_absolute_error_sums += mean_absolute_error

        # Get the confusion matrix
        tp = len(target_answers.intersection(set(top_5_responses)))
        fn = len(target_answers.difference(set(top_5_responses)))
        fp = len(set(top_5_responses).difference(target_answers))
        tn = len(answers_df) - tp - fn - fp

        total_tp += tp
        total_fn += fn
        total_fp += fp
        total_tn += tn

    total_precision = total_tp / (total_tp + total_fp)
    total_recall = total_tp / (total_tp + total_fn)
    total_f1 = 2 * ((total_precision * total_recall) / (total_precision + total_recall))

    number_of_queries = len(questions_df.index)
    total_mean_average_precision = mean_average_precision_sums / number_of_queries
    total_mean_reciprocal_rank = mean_reciprocal_rank_sums / number_of_queries
    total_normalised_discounted_cumulative_gain = normalised_discounted_cumulative_gain_sums / number_of_queries
    # Cannot calculate mean absolute error when using BERT as scores are not in range 0-1
    total_mean_absolute_error = mean_absolute_error_sums / number_of_queries

    csv_row = csv_template.substitute(whoosh_extraction_model=whoosh_extraction_model,
                                      whoosh_expansion_model=whoosh_expansion_model,
                                      whoosh_query_grouping=whoosh_query_grouping,
                                      keyword_threshold=filter_n, keyword_expander_n=expander_n,
                                      total_precision=total_precision, total_recall=total_recall,
                                      total_f1=total_f1, mean_average_precision=total_mean_average_precision,
                                      mean_reciprocal_rank=total_mean_reciprocal_rank,
                                      normalised_discounted_cumulative_gain=total_normalised_discounted_cumulative_gain,
                                      mean_absolute_error=total_mean_absolute_error)

    with open(RESULTS_FILEPATH, 'a') as out:
        out.write(csv_row)


def get_top_5_responses_infersent(filtered_responses, question) -> tuple:
    # Use InferSent to calculate similarities between questions and answers
    rated_responses = []
    for response in filtered_responses:
        similarity = sentence_handler.similarity_infersent(question, response)
        rated_responses.append({"response": response, "score": similarity})

    # Sort the responses based on their similarity score, most similar at the top
    rated_responses = sorted(rated_responses, key=lambda x: x["score"], reverse=True)

    # Get the top 5 responses
    top_5_responses = [x["response"] for x in rated_responses[:5]]
    top_5_responses_with_score = rated_responses[:5]

    return top_5_responses, top_5_responses_with_score


def get_top_5_responses_bert(filtered_responses, question) -> tuple:
    # Use BERT to calculate similarities between questions and answers
    unnormalised_scores = sentence_handler.similarity_bert(question, filtered_responses)
    # Get ideal score & normalise
    ideal_score = sentence_handler.similarity_bert(question, [question])
    scores = np.divide(unnormalised_scores, ideal_score)

    ranking_ids = np.argsort(scores)[::-1][:5]
    top_5_responses_with_score = []
    top_5_responses = []

    for id in ranking_ids:
        top_5_responses.append(filtered_responses[id])
        top_5_responses_with_score.append({"response": filtered_responses[id], "score": scores[id]})

    return top_5_responses, top_5_responses_with_score


def calculate_metrics(top_5_responses_with_score, relevancy_scores_with_answer_text, question_id):
    # Mean absolute error calculation & Mean average precision (MAP) & Mean reciprocal rank (MRR) &
    # Normalised discounted cumulative gain (NDCG) + Ideal/ground truth for DCG - used for calculating normalised DCG
    intermediate_sum_mae, intermediate_sum_map, intermediate_sum_mrr, discounted_cumulative_gain, \
        discounted_cumulative_gain_ideal, relevant_responses_counter = 0, 0, 0, 0, 0, 0

    for response in top_5_responses_with_score:
        # Retrieve corresponding actual relevancy score for Q & A combination
        actual_relevancy_score = [x['score'] for x in relevancy_scores_with_answer_text
                                  if (x['question_id'] == question_id and x['response'] == response['response'])][0]

        # Sum absolute difference
        intermediate_sum_mae += abs(response['score'] - actual_relevancy_score)

        # Get the rank (add one as index starts at 0)
        rank = top_5_responses_with_score.index(response) + 1

        # Calculate discounted gain of response- if first item in ranked list, DG = relevancy score
        if top_5_responses_with_score.index(response) == 0:
            discounted_gain = actual_relevancy_score
            discounted_gain_ideal = 1
        else:
            discounted_gain = actual_relevancy_score / np.log2(rank)
            discounted_gain_ideal = 1 / np.log2(rank)

        # Accumulate discounted gain for each response
        discounted_cumulative_gain += discounted_gain
        discounted_cumulative_gain_ideal += discounted_gain_ideal

        # If response is relevant then calculate its precision & reciprocal rank
        if actual_relevancy_score == 1:
            relevant_responses_counter += 1
            precision = relevant_responses_counter / rank
            reciprocal_rank = 1 / rank

            # Sum precision & reciprocal rank values
            intermediate_sum_map += precision
            intermediate_sum_mrr += reciprocal_rank

    # Calculate average precision for query/question - if no relevant response then this should be 0
    if relevant_responses_counter == 0:
        query_average_precision = 0
        query_average_reciprocal_rank = 0
    else:
        query_average_precision = intermediate_sum_map / relevant_responses_counter
        query_average_reciprocal_rank = intermediate_sum_mrr / relevant_responses_counter

    # Calculate the normalised discounted cumulative gain for this query/question
    # Note: DCG of ideal/ground truth has been calculated along the way,
    # this is where all returned top k responses are of the highest relevancy-
    # hardcoded as with common english questions dataset we know each question
    # has atleast 5 responses of relevancy 1
    normalised_discounted_cumulative_gain = discounted_cumulative_gain / discounted_cumulative_gain_ideal if discounted_cumulative_gain_ideal > 0 else 0

    # Mean Absolute error for query/question
    # Divide by number of scores
    num_returned_responses = len(top_5_responses_with_score)
    mean_absolute_error = intermediate_sum_mae / num_returned_responses if num_returned_responses > 0 else 1

    return query_average_precision, query_average_reciprocal_rank, normalised_discounted_cumulative_gain, \
        mean_absolute_error


def start_user_input_comparison():
    new_sentence = input("Enter a sentence:\n")

    while True:
        # Keep comparing new sentence to the old until user does not provide one
        old_sentence = new_sentence
        new_sentence = input("Enter another sentence:\n")

        similarity = sentence_handler.similarity(new_sentence, old_sentence)
        keywords = sentence_handler.extract_keywords(new_sentence)
        expanded_keywords = keyword_expander.expand(keywords)

        print("Similarity to prev sent: {}".format(similarity))
        print("Keywords: {}".format(keywords))
        print("Keywords+: {}".format(expanded_keywords))

        _, _ = sentence_handler.visualize(new_sentence)

        if new_sentence == "":
            break


if __name__ == "__main__":
    with open(RESULTS_FILEPATH, 'w') as out:
        out.write("whoosh_extraction_model, whoosh_expansion_model, whoosh_query_grouping, "
                  "keyword_threshold,keyword_expander_n, total_precision,"
                  "total_recall,total_f1, mean_average_precision, mean_reciprocal_rank, "
                  "normalised_discounted_cumulative_gain, mean_absolute_error\n")

    print("Generating results...")
    if FILTER_BY_KEYWORDS:
        if USE_WHOOSH:
            models = [Bo1Model, Bo2Model, KLModel]
            groups = [AndGroup, OrGroup]
            for model in models:
                for expansion_model in models:
                    for group in groups:
                        for numterms_initial_query in [1, 2, 3, 5, 8, 13, 21, 34, 55, 89]:
                            for numterms_query_expansion in [0, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]:
                                compare_from_files(QUESTIONS_FILEPATH, ANSWERS_FILEPATH,
                                                   RELEVANCY_SCORES_FILEPATH, numterms_initial_query, numterms_query_expansion, model, expansion_model, group)
                                # if not expanding keywords continue with the execution of the outer loop.
                                if not EXPAND_KEYWORDS:
                                    break
        else:
            for keyword_threshold in [1, 2, 3, 5, 8, 13, 21]:
                for keyword_expander_n in [0, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]:
                    sentence_handler.set_keyword_threshold(keyword_threshold)
                    keyword_expander.set_n(keyword_expander_n)

                    compare_from_files(QUESTIONS_FILEPATH, ANSWERS_FILEPATH,
                                       RELEVANCY_SCORES_FILEPATH, keyword_threshold, keyword_expander_n, None, None, None)

                    # if not expanding keywords continue with the execution of the outer loop.
                    if not EXPAND_KEYWORDS:
                        break
    else:
        compare_from_files(QUESTIONS_FILEPATH, ANSWERS_FILEPATH,
                           RELEVANCY_SCORES_FILEPATH, 0, 0, None, None, None)

    print("Done.")
