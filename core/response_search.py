import re
from typing import List
import itertools


class ResponseSearcher:
    """Searches for responses that contain keywords inside the responses"""

    def __init__(self, responses: List[str]):
        """

        :param responses: Dat
        """
        self.responses = responses

    def set_responses(self, responses):
        """
        :param responses: New response responses
        :return: None
        """
        self.responses = responses

    def search(self, keywords):
        """Search for keywords in the response dataset to find responses that match

        :param keywords: A list of keywords
        :return: A list of matched responses in descending order of number of matches
        """
        results = []
        results_with_matches = []

        for response in self.responses:
            matches = 0

            for keyword in keywords:
                # Match exact word not substring e.g. 'hi' would not match in 'this is wrong'
                match = re.search(r'\b' + re.escape(keyword) + r'\b', response, re.IGNORECASE)
                if match:
                    matches += 1

            if matches > 0:
                results_with_matches.append({"response": response, "number_of_keywords_matched": matches})

        # Order matches in descending order of number of keywords matched
        ordered_results_with_matches = sorted(results_with_matches, key=lambda k: k['number_of_keywords_matched'], reverse=True)
        for results_with_matches in ordered_results_with_matches:
            results.append(results_with_matches["response"])

        return results
