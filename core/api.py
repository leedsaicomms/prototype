from string import Template

import pandas as pd
import numpy as np
from sanic import Sanic
from sanic.response import json
from sanic_cors import cross_origin
from whoosh.classify import *
from whoosh.qparser import OrGroup, AndGroup

from constants import *
from keyword_expansion import KeywordExpander
from response_search import ResponseSearcher
from sentence_handling import SentenceHandler
from whoosh_search_engine import WhooshSearchEngine

api = Sanic()

# A all_responses of responses, which is a CSV file in this prototype
all_responses = pd.read_csv(ANSWERS_FILEPATH, encoding="utf-8").text

# Initialize necessary components
sentence_handler = SentenceHandler(SENTENCE_HANDLER_WV_PATH, MODEL_PARAMS) if not USE_WHOOSH else None
sentence_handler.set_keyword_threshold(KEYWORD_THRESHOLD) if not USE_WHOOSH else None
keyword_expander = KeywordExpander(KEYWORD_EXPANDER_WV_PATH, KEYWORD_EXPANDER_N) if not USE_WHOOSH else None
response_searcher = ResponseSearcher(all_responses)
search_engine = WhooshSearchEngine()

# For debugging, template for printing answer and similarity score
pt = Template("${response}\t${score}")


@api.post("/responses")
@cross_origin(api)
async def get_top5_responses(request):
    """
    Generates and returns a set of responses given a question.

    :param request: sanic's Request object, containing headers and the body
    :return: a JSON response, either containing an error or the most relevant responses
    """
    # Extract the question from the request body
    question = request.json.get("question", None)

    # Return an error response if no question was provided.
    if question is None or not (type(question) is str):
        return json({"status": "ERROR", "message": "EMPTY_OR_INVALID_QUESTION_FIELD"}, status=400)

    # Obtain a list of tuples containing possible responses and their similarities
    responses = await get_top_n_responses(question, 5, debug=DEBUG)

    # Return the responses in JSON format
    # An attempt was made to return both the responses and the scores,
    # but the result was recursion depth error

    return json({"responses": [x["response"] for x in responses]}, status=200) if responses else json({"responses": []}, status=200)


async def get_top_n_responses(question, n, debug=False):
    """
    Computes the n top response
    :param question: the question for which we want responses
    :param n: the number of responses
    :param debug: If true, show additional debug output
    :return: a list of top responses and their similarity scores
    """

    # Extract and expand keywords
    if FILTER_BY_KEYWORDS:
        if USE_WHOOSH:
            keywords = search_engine.extract_keywords(question, numterms=KEYWORD_THRESHOLD, model=MODEL)
        else:
            keywords = sentence_handler.extract_keywords(question)

        if EXPAND_KEYWORDS:
            if USE_WHOOSH:
                keywords = search_engine.expand_keywords(keywords, numterms=KEYWORD_EXPANDER_N, group=GROUP, model=EXPANSION_MODEL)
            else:
                keywords = keyword_expander.expand(keywords, debug=debug)
            
        # Search for keywords in the all_responses to get a smaller subset of responses
        filtered_responses = response_searcher.search(keywords)

    else:
        filtered_responses = all_responses

    if COMPUTE_SIMILARITY:
        if USE_BERT_FOR_SIMILARITY:
            return get_top_n_responses_bert(question, list(filtered_responses), n)
        else:
            return get_top_n_responses_infersent(question, filtered_responses, n)
    else:
        responses = []
        for response in filtered_responses[:5]:
            responses.append({"response": response, "score": 1})
        return responses


def get_top_n_responses_infersent(question: str, filtered_responses: set, n: int) -> list:
    """
    Computes top n responses using infersent for semantic similarity scoring
    :param question: the question for which we want responses
    :param filtered_responses: potential responses filtered via keyword search
    :param n: number of responses
    :return: a list of top responses and their similarity scores
    """
    rated_responses = []

    # Generate and record similarity scores for every response in the all_responses
    for response in filtered_responses:
        similarity = sentence_handler.similarity_infersent(question, response)
        rated_responses.append({"response": response, "score": similarity})

    # Sort the responses based on their similarity score, most similar at the top
    rated_responses = sorted(rated_responses, key=lambda x: x["score"], reverse=True)

    # Debug - print answers and scores
    for response in rated_responses:
        print(pt.substitute(response=response["response"], score=response["score"]))

    return rated_responses[:n]


def get_top_n_responses_bert(question: str, filtered_responses: list, n: int) -> list:
    """
    Computes top n responses using infersent for semantic similarity scoring
    :param question: the question for which we want responses
    :param filtered_responses: potential responses filtered via keyword search
    :param n: number of responses
    :return: a list of top responses and their similarity scores
    """
    rated_responses = []
    scores = sentence_handler.similarity_bert(question, filtered_responses)
    ranking_ids = np.argsort(scores)[::-1][:n]
    for id in ranking_ids:
        rated_responses.append({"response": filtered_responses[id], "score": scores[id]})

    return rated_responses


if __name__ == "__main__":
    api.run(host="localhost", port=5678)
