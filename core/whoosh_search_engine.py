import os.path
import pandas as pd
from constants import *
from typing import List
from whoosh.fields import Schema, TEXT
from whoosh.index import create_in
from whoosh.qparser import QueryParser, OrGroup, AndGroup
from whoosh.classify import *


class WhooshSearchEngine:
    """Provides API for extracting and expanding keywords from a query"""

    def __init__(self):
        if not os.path.exists("index"):
            os.mkdir("index")
        schema = Schema(content=TEXT(stored=True))
        self.index = create_in("index", schema)
        self.populate_index()

    def populate_index(self):
        writer = self.index.writer()
        documents: List[str] = pd.read_csv(ANSWERS_FILEPATH).text
        for document in documents:
            writer.add_document(content=document)
        writer.commit()

    def extract_keywords(self, text: str, numterms: int = 5, model=Bo1Model) -> List[str]:
        """
        :param text: query input
        :param numterms: max number of desired keywords
        :param model: whoosh.classify object model to be used for extracting keywords
        :return: list of keyword strings
        """

        with self.index.searcher() as searcher:
            try:
                keywords = [keyword for keyword, score in searcher.key_terms_from_text("content", text, numterms=numterms, normalize=True, model=model)]
            except ZeroDivisionError:
                keywords = []
            return keywords

    def expand_keywords(self, keywords: List[str], numterms: int = 5, group=AndGroup, model=Bo1Model) -> List[str]:
        """
        :param keywords: list of keywords to be expanded
        :param numterms: max number of desired keywords
        :param group: querying method of keywords e.g. AndGroup: keyword1 AND keyword2, OrGroup: keyword1 OR keyword2
        :return: list of keywords (includes original + more if expansion successful, else is just original keywords)
        """

        with self.index.searcher() as searcher:
            parser = QueryParser("content", schema=self.index.schema, group=group)
            results = searcher.search(parser.parse(keywords.__str__()))
            expanded_keywords: List[str] = [keyword for keyword, score in results.key_terms("content", docs=self.index.doc_count(), numterms=numterms, model=model)]
        # Note that the expanded keywords won't contain the original if it doesn't find any extra keywords
        return expanded_keywords if expanded_keywords else keywords

