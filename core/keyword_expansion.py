from abc import ABC, abstractmethod
from typing import Union, List, Set

from gensim import models


class AbstractKeywordExpander(ABC):
    @abstractmethod
    def expand(self, keywords: Union[List[str], Set[str]]) -> List[str]:
        pass

    @abstractmethod
    def set_n(self, keyword_expander_n):
        pass


class DummyKeywordExpander(AbstractKeywordExpander):
    def expand(self, keywords: Union[List[str], Set[str]]) -> List[str]:
        return list(keywords)

    def set_n(self, keyword_expander_n):
        pass


class KeywordExpander(AbstractKeywordExpander):
    """Expands keywords into words that are related to the original keywords"""

    word_vectors: models.FastText

    def __init__(self, word_vector_filepath: str = None, n: int = 5):
        """
        :param word_vector_filepath: Word vector (embedding) filepath
        :param n: How many extra words add for each word during expansion
        """
        self.n = n

        if word_vector_filepath:
            print("Loading word vectors...")
            self.word_vectors = models.KeyedVectors.load_word2vec_format(word_vector_filepath, binary=False)
            print("Done.")
        else:
            self.word_vectors = None

    def set_n(self, n):
        self.n = n
        return self

    def expand(self, keywords: Union[List[str], Set[str]], debug=False) -> List[str]:
        """Expand keyword list into

        :param keywords: A set of keywords
        :param debug: If true, show additional debug output
        :return:
        """

        expanded_keywords = {keyword.lower() for keyword in keywords}
        if self.word_vectors:
            for keyword in keywords:
                # Expand keyword list with topn similar words
                expanded_keywords.update({word.lower() for word, similarity in
                                          self.word_vectors.most_similar(positive=keyword, topn=self.n)})

            print(expanded_keywords) if debug else None

        return list(expanded_keywords)
