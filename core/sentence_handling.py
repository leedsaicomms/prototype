import re
from abc import ABC, abstractmethod
from typing import Tuple, List, Dict, Any

import numpy as np
import torch

from infersent.models import InferSent
from infersent.model_loading_utils import load_model
from bert_serving.client import BertClient


class AbstractSentenceHandler(ABC):
    @abstractmethod
    def extract_keywords(self, sent: str, word_weights: Dict[str, float] = None) -> List[str]:
        pass

    @abstractmethod
    def similarity_infersent(self, sent1: str, sent2: str) -> float:
        pass

    @abstractmethod
    def similarity_bert(self, sent: str, sentences: list) -> np.ndarray:
        pass

    @abstractmethod
    def visualize(self, sent: str) -> Tuple[Any, Any]:
        pass

    @abstractmethod
    def weigh_words(self, sent: str) -> Dict[str, float]:
        pass

    @abstractmethod
    def set_keyword_threshold(self, keyword_threshold):
        pass


class DummySentenceHandler(AbstractSentenceHandler):
    """Sentence handler that does not use any actual embeddings.
    Used for debugging to save time loading the embeddings"""

    def weigh_words(self, sent: str) -> Dict[str, float]:
        return {word.lower(): 10.0 for word in re.findall(r'\b\w+\b', sent)}

    def visualize(self, sent: str) -> Tuple[Any, Any]:
        return None, None

    def similarity(self, sent1: str, sent2: str) -> float:
        return 0.0

    def extract_keywords(self, sent: str, word_weights: Dict[str, float] = None) -> List[str]:
        return list(self.weigh_words(sent).keys())

    def set_keyword_threshold(self, keyword_threshold):
        pass


class SentenceHandler(AbstractSentenceHandler):
    """Handles embeddings using InferSent as a backend"""
    model: InferSent

    def __init__(self, wv_filepath: str, model_params, vocab_size: int = 1000000,
                 keyword_threshold: float = 8.0):
        """
        :param wv_filepath: Word vector filepath
        :param vocab_size: Number of most frequent words
        :param model_params: InferSent model parameters
        :param keyword_threshold: Default threshold for keywords
        """
        self.keyword_threshold = keyword_threshold
        self.model = None
        self.load_model(wv_filepath, model_params, vocab_size)

    def set_keyword_threshold(self, keyword_threshold):
        self.keyword_threshold = keyword_threshold
        return self

    def weigh_tokens(self, sent: str, tokenize: bool = True) -> Dict:
        """Get token importance weights for each token in a sentence. Based of InferSent.visualize

        :param sent: A sentence
        :param tokenize: True, if MOSES tokenization should be used
        """

        sent = sent.split() if not tokenize else self.model.tokenize(sent)
        sent = [[self.model.bos] + [word for word in sent if word in self.model.word_vec] + [self.model.eos]]

        if ' '.join(sent[0]) == '%s %s' % (self.model.bos, self.model.eos):
            import warnings
            warnings.warn('No words in "%s" have w2v vectors. Replacing \
                                   by "%s %s"..' % (sent, self.model.bos, self.model.eos))
        batch = self.model.get_batch(sent)

        if self.model.is_cuda():
            batch = batch.cuda()

        output = self.model.enc_lstm(batch)[0]
        output, idxs = torch.max(output, 0)
        idxs = idxs.data.cpu().numpy()
        argmaxs = [np.sum((idxs == k)) for k in range(len(sent[0]))]

        weights = [100.0 * n / np.sum(argmaxs) for n in argmaxs]

        return {sent[0][i]: weights[i] for i in range(len(sent[0]))}

    def weigh_words(self, sent: str) -> Dict:
        """Get importance weights for each word in a sentence
        
        :param sent: A sentence
        """

        # Get tokens and their weights
        token_weights = self.weigh_tokens(sent)

        # Remove non-word tokens
        regex = re.compile('[^\w-]')
        word_weights = {}
        for token, weight in token_weights.items():
            if regex.search(token) is None:
                word_weights[token] = weight

        return word_weights

    def extract_keywords(self, sent: str, word_weights: Dict[str, float] = None) -> List[str]:
        """Extract keywords from a sentence

        :param sent: A sentence. Irrelevant if word_weights is specified.
        :param word_weights: Optional. Pre-computed word weights for keyword extraction.
        """

        if not word_weights:
            word_weights = self.weigh_words(sent)

        keywords = [word for word, weight in word_weights.items() if weight > self.keyword_threshold]
        return keywords

    def visualize(self, sent, tokenize=True) -> Tuple[Any, Any]:
        """Visualize a sentence

        :param sent: A sentence
        :param tokenize: True, if MOSES tokenization should be used
        """

        return self.model.visualize(sent, tokenize)

    def load_model(self, wv_filepath: str, model_params: dict, vocab_size: int = 1000000) -> None:
        """Load InferSent model

        :param wv_filepath: Word vector filepath
        :param model_params: InferSent model parameters
        :param vocab_size: Number of most frequent words that vocabulary has
        """

        print("Loading InfraSent model...")
        print(model_params)

        # Load model
        self.model = load_model(model_params)

        # If infersent1 -> use GloVe embeddings. If infersent2 -> use InferSent embeddings.
        self.model.set_w2v_path(wv_filepath)

        # Load embeddings of K most frequent words
        self.model.build_vocab_k_words(K=vocab_size)
        print("Done.")

    def similarity_infersent(self, sent1: str, sent2: str) -> float:
        """Compare similarity of 2 sentences
        :param sent1: First sentence
        :param sent2: Second sentence

        :return Similarity score [0-1]
        """

        return self._cos_similarity(self.model.encode(sent1)[0], self.model.encode(sent2)[0])

    @staticmethod
    def _cos_similarity(u, v) -> float:
        """Get cosine similarity of 2 vectors

        :param u: First vector
        :param v: Second vector

        :return Similarity score [0-1]
        """
        return float(np.dot(u, v) / (np.linalg.norm(u) * np.linalg.norm(v)))

    def similarity_bert(self, sent: str, sentences: list) -> np.ndarray:
        """Compare similarity of 1 sentence against a list of other sentences, using BERT
        :param sent: single sentence
        :param sentences: list of sentences

        :return list of similarity vectors
        """
        bc = BertClient()
        sent_vec = bc.encode([sent])[0]
        sentences_vec = bc.encode(sentences)

        return np.sum(sent_vec * sentences_vec, axis=1)