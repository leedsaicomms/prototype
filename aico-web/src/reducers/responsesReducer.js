import {GET_RESPONSES, RESET_RESPONSES} from "../actions/types";

const initialState = [];

export default function(state = initialState, action) {
    switch(action.type) {
        case GET_RESPONSES:
            return action.payload;
        case RESET_RESPONSES:
            return initialState;
        default:
            return state;
    }
}