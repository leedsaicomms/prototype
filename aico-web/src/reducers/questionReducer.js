import {RESET_QUESTION, STORE_QUESTION} from "../actions/types";

const initialState = '';

export default function(state = initialState, action) {
    switch(action.type) {
        case STORE_QUESTION:
            return action.payload;
        case RESET_QUESTION:
            return initialState;
        default:
            return state;
    }
}