import {STORE_QUESTION} from "./types";

export const storeQuestionAction = (question) => (dispatch) => {
    return new Promise((resolve, reject) => {
        if (question) {
            dispatch({
                type: STORE_QUESTION,
                payload: question
            });

            resolve(question);
        } else {
            reject("No question selected");
        }
    });
};