import {GET_RESPONSES} from "./types";

export const getResponsesAction = (question) => (dispatch) => {
    return fetch('http://localhost:5678/responses', {method: "POST", body: JSON.stringify({question: question})})
        .then((response) => {
            return response.json();
        })
        .then((body) => {
            if (!body || !body.responses) {
                console.log("Body: "+body);
                return Promise.reject();
            }

            dispatch({
                type: GET_RESPONSES,
                payload: body.responses
            });

            return Promise.resolve();
        });
};