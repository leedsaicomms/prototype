export const STORE_QUESTION = 1;
export const GET_RESPONSES = 2;
export const RESET_QUESTION = 3;
export const RESET_RESPONSES = 4;