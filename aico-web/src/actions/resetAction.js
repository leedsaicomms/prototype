import {RESET_QUESTION, RESET_RESPONSES} from "./types";

export const resetAction = () => (dispatch) => {
    return new Promise((resolve) => {
        dispatch({
            type: RESET_QUESTION
        });

        dispatch({
            type: RESET_RESPONSES
        });

        resolve();
    });
};