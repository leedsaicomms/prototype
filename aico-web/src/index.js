import 'font-awesome/css/font-awesome.min.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import  React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { register }  from './serviceWorker';

ReactDOM.render(<App />, document.getElementById('root'));

register();
