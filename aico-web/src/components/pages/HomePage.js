import React, {Component} from 'react';
import {connect} from 'react-redux';
import ListView from '../views/ListView';
import PropTypes from 'prop-types';
import {storeQuestionAction} from "../../actions/storeQuestionAction";
import {getResponsesAction} from "../../actions/getResponsesAction";

class HomePage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            question: '',
            response: '',

            // For the prototype we have a fixed set of questions
            questions: [
                "What do you do?",
                "Are you married?",
                "What do you do in your free time?",
                "What's the weather like?",
                "What time is it?",
                "How are you feeling?",
                "How was your day?",
                "Did you like the movie?",
                "What have you been up to lately?",
                "What's the matter?"
            ]
        };

        this.onClick = this.onClick.bind(this);
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md">
                        <h2>Select a question:</h2>
                    </div>
                </div>
                <ListView onClick={this.onClick} items={this.state.questions}/>
            </div>
        );
    }

    onClick(event) {
        event.preventDefault();

        this.props.storeQuestionAction(event.target.innerHTML)
            .then(question => {
                return this.props.getResponsesAction(question)
            })
            .then(() => {
                this.props.history.push('/responses');
            })
            .catch(() => {
                alert("Sorry, an unknown error has occurred.");
            });
    }
}

const mapStateToProps = (state) => ({
    question: state.question,
    responses: state.responses
});

HomePage.propTypes = {
    storeQuestionAction: PropTypes.func.isRequired,
    getResponsesAction: PropTypes.func.isRequired
};

export default connect(mapStateToProps, {storeQuestionAction, getResponsesAction})(HomePage);