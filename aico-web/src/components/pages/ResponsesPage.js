import React, {Component} from 'react';
import {connect} from 'react-redux';
import ListView from '../views/ListView';
import PropTypes from 'prop-types';
import {storeQuestionAction} from "../../actions/storeQuestionAction";
import {getResponsesAction} from "../../actions/getResponsesAction";
import {resetAction} from "../../actions/resetAction";

class ResponsesPage extends Component {
    constructor(props) {
        super(props);

        this.onClick = this.onClick.bind(this);
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md">
                        <h2>Your Question:</h2>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md">
                        {this.props.question}
                    </div>
                </div>
                <br/>
                <div className="row">
                    <div className="col-md">
                        <h2>Responses:</h2>
                    </div>
                </div>
                <ListView onClick={this.onClick} items={this.props.responses}/>
            </div>
        );
    }

    onClick(event) {
        event.preventDefault();

        this.props.resetAction()
            .then(() => {
                this.props.history.push('/');
            });
    }
}

const mapStateToProps = (state) => ({
    question: state.question,
    responses: state.responses
});

ResponsesPage.propTypes = {
    resetAction: PropTypes.func.isRequired
};

export default connect(mapStateToProps, {resetAction})(ResponsesPage);