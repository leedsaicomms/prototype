import React, {Component} from 'react';
import PropTypes from 'prop-types';

export default class ListView extends Component {
    static propTypes = {
        onClick: PropTypes.func.isRequired,
        items: PropTypes.array.isRequired,
    };

    render() {
        return (
            <ul className="list-group">
                {this.props.items.map((item, i) =>
                    <li key={i} className={"list-item-group"} onMouseDown={() => {
                        document.body.style.cursor = "default";
                    }} onClick={this.props.onClick} onMouseEnter={() => {
                        document.body.style.cursor = "pointer";
                    }} onMouseLeave={() => {
                        document.body.style.cursor = "default";
                    }}>{item}</li>
                )}
            </ul>
        );
    }
}