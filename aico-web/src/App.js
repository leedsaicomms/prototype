import React, { Component } from 'react';
import './App.css';

import HomePage from './components/pages/HomePage';

import { BrowserRouter as Router, Route } from 'react-router-dom';
import store from "./store";
import { Provider } from 'react-redux';
import ResponsesPage from "./components/pages/ResponsesPage";

class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <Router>
                <   div id="app">
                        <Route exact path="/" component={HomePage} />
                        <Route exact path="/responses" component={ResponsesPage} />
                    </div>
                </Router>
            </Provider>
        );
    }
}

export default App;
