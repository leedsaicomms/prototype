# Leeds AI Comms Prototype (PROTO)

## Installation
1. Download project dependencies using `pip3 install -r core/requirements.txt` and in directory: aico-web/ run: `npm install`
2. Download GloVe and fastText datasets (assuming you have some directory 'dataset'): 
    ```
    mkdir dataset/GloVe
    curl -Lo dataset/GloVe/glove.840B.300d.zip http://nlp.stanford.edu/data/glove.840B.300d.zip
    unzip dataset/GloVe/glove.840B.300d.zip -d dataset/GloVe/
    mkdir dataset/fastText
    curl -Lo dataset/fastText/crawl-300d-2M.vec.zip https://s3-us-west-1.amazonaws.com/fasttext-vectors/crawl-300d-2M.vec.zip
    unzip dataset/fastText/crawl-300d-2M.vec.zip -d dataset/fastText/
    ```
3. Set `DATASET_PATH` environment variable to your InferSent's dataset directory using `
export DATASET_PATH=/path/to/dataset`. In this directory you should have fastText and GloVe directories.
4. Convert GloVe embeddings to w2v format for Gensim
    ```
    cd $DATASET_PATH/GloVe
    python3 -m gensim.scripts.glove2word2vec --input glove.840B.300d.txt -o glove.840B.300d.w2v.txt
5. Download one of the BERT pre-trained models at: https://github.com/hanxiao/bert-as-service/#1-download-a-pre-trained-bert-model (suggested is uncased BERT base)   


## Running
Start the backend & frontend (+ bert as a service server if using BERT), then visit URL: localhost:3000

##### Frontend
Terminal: in directory: 'prototype/aico-web'
`npm start`

##### Backend
Terminal: in directory 'prototype/core':
`python3 api.py`

##### Toggling between InferSent and BERT
Toggle constant USE_BERT_FOR_SIMILARITY in constants.py

###### BERT
Run the server: `bert-serving-start -model_dir uncased_L-12_H-768_A-12/ -num_worker=1` edit the model directory to point to your directory of the model

##### Changing Parameters
Edit constants in constants.py:
* FILTER_BY_KEYWORDS (bool)
* KEYWORD_THRESHOLD (int)
* EXPAND_KEYWORDS (bool) 
* KEYWORD_EXPANDER_N (int)

#### Evaluation
Terminal: in directory 'prototype/core': `python3 eval.py`
